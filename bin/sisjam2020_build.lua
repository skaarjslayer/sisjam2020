local object={}
object.__index=object
function object:extend()
local new={}
for k, v in pairs(self) do
if sub(k,1,2)=="__" then
new[k] = v
end
end
new.__index = new
setmetatable(new, self)
return new
end
function object:__call(...)
local obj = setmetatable({}, self)
obj:new(...)
return obj
end
function object:merge(table)
for k,v in pairs(table) do
self[k] = v
end
end
state_machine=object:extend()
function state_machine:new()
-- vars
local m_sts, m_cst = {}, nil
-- properties
function self.current_state()
return m_cst
end
-- functions
function self.update()
if m_cst then
if m_cst.update then
m_cst.update()
end
end
end
function self.draw()
if m_cst then
if m_cst.draw then
m_cst.draw()
end
end
end
function self.push(a_nst)
if m_cst then
if m_cst.disable then
m_cst.disable()
end
end
m_sts[#m_sts + 1]=a_nst
m_cst=a_nst
if m_cst.enter then
m_cst.enter()
end
if m_cst.enable then
m_cst.enable()
end
end
function self.advance(a_nst)
if m_cst then
if m_cst.disable then
m_cst.disable()
end
if m_cst.exit then
m_cst.exit()
end
del(m_sts, m_cst)
end
m_sts[#m_sts + 1]=a_nst
m_cst=a_nst
if m_cst.enter then
m_cst.enter()
end
if m_cst.enable then
m_cst.enable()
end
end
end
debug_collision = false
collision_partition = nil
trigger_partition = nil
player_ref=nil
camera_ref=nil
levels={}
level=nil
next_level=nil
spawn_pos=nil
text_boxes={}
function _init()
    setup_level_data()
    next_level = levels[2]
    spawn_pos=vector(48,64)
end
function _update()
    if next_level then
        level = next_level
        level.init()
        next_level = nil
        player_ref=player(spawn_pos)
        camera_ref=player_camera(spawn_pos)
    end
    
    if #text_boxes > 0 then
        -- Text box will block other operations
        if btnp(4) then deli(text_boxes,1) end
    else
        player_ref.update()
        camera_ref.update()
    end
end
function _draw()
    camera_ref.draw()
    
    cls()
    palt(14, true)
    
    level.draw()
    palt(14, true)
    
    player_ref.draw()
    
    level.overdraw()
    
    if debug_collision then
        collision_partition.draw(3)
        trigger_partition.draw(4)
    end
    
    if #text_boxes > 0 then
        camera()
        draw_text_box(text_boxes[1],1,12)
    end
end
function setup_level_data()
    levels[1]=level(0,0,0,0,16,16)
    levels[1].add_interactable(48,16,interactable(box(48,16,8,8),load_level,{num=2,x=48,y=32}))
    levels[1].add_interactable(80,72,interactable(box(80,72,8,8),show_text_boxes,{"this is a chest.", "duh!"}))
    levels[2]=level(16,0,0,0,12,17)
    levels[2].add_interactable(48,24,interactable(box(48,24,8,8),load_level,{num=1,x=48,y=32}))
    return
end
function load_level(lvl_dat)
    next_level = levels[lvl_dat.num]
    spawn_pos = vector(lvl_dat.x,lvl_dat.y)
    level.init()
end
function show_text_boxes(tbs)
    text_boxes={}
    for i,v in ipairs(tbs) do add(text_boxes, v) end
end
function draw_text_box(text, box_c, text_c)
    --wdith 96, height 24
    rectfill(16,52,112,76,box_c)
    print(text,24,60,text_c)
end
interactable=object:extend()
function interactable:new(p_box, p_action, p_param)
    local m_box, m_action, m_param = p_box, p_action, p_param
    function self.interact()
        m_action(m_param)
    end
    -- Passtrhrough call to box
    function self.tl()
        return m_box.tl()
    end
    
    -- Passtrhrough call to box
    function self.br()
        return m_box.br()
    end
    -- Passtrhrough call to box
    function self.update(a_x,a_y)
        m_box.update(a_x,a_y)
end
    
    -- Passtrhrough call to box
    function self.check(a_o,a_xv,a_yv)
        m_box.check(a_o,a_xv,a_yv)
    end
    -- Passtrhrough call to box
    function self.draw(c)
        m_box.draw(c)
    end
end
level=object:extend()
function level:new(na_cx, na_cy, na_sx, na_sy, na_w, na_h)
    local cellx = na_cx
    local celly = na_cy
    local screenx = na_sx
    local screeny = na_sy
    local width = na_w
    local height = na_h
    local m_collision_partition=partition(na_w*8,na_h*8,na_w,na_h)
    local m_trigger_partition=partition(na_w*8,na_h*8,na_w,na_h)
    local player_spawn_position = vector()
    --properties
    function self.player_spawn_position(v)
        if (v) player_spawn_position=v
        return player_spawn_position
    end
    for x=0,na_w-1 do
        for y=0,na_h-1 do
            local sprite = mget(na_cx+x,na_cy+y)
            local collision_flag = fget(sprite, 1)
            local nx=x*8
            local ny=y*8
            if collision_flag then
                m_collision_partition.place(nx, ny, box(nx,ny,8,8))
            end
            if sprite==185 then
                player_spawn_position = vector(nx,ny)
            end
        end
    end
    function self.add_interactable(posx, posy, inter)
        m_trigger_partition.place(posx, posy, inter)
    end
    function self.init()
        collision_partition = m_collision_partition
        trigger_partition = m_trigger_partition
    end
    function self.draw()
        map(cellx, celly, screenx, screeny, width, height, 0x1)
        pal(2,1)
        map(cellx, celly, screenx, screeny, width, height, 0x80)
        pal()
    end
    function self.overdraw()
        map(cellx, celly, screenx, screeny, width, height, 0x4)
    end
end
player_camera=object:extend()
function player_camera:new(pos)
    local position=pos-vector(64, 64)
    local velocity=vector()
    local direction=vector()
    local target_position=vector()
    local move_force = 5
    local mass = 1
    local max_acceleration = 0.2
    local max_speed = 2
    
    function self.update()
        target_position = player_ref.position() - vector(64, 64)
        local diff_vector = target_position - position
        local direction = -diff_vector
        local desired_velocity = direction * vector(move_force, move_force)
        local acceleration = desired_velocity - velocity
        acceleration = acceleration.truncate(max_acceleration)
        acceleration /= vector(mass, mass)
        -- apply acceleration and cap at max speed
        velocity += acceleration
        velocity = velocity.truncate(max_speed)
        if #diff_vector<16 then
            velocity = vector()
        end
        position += velocity
    end
    function self.draw()
        camera(position.x(),position.y())  
    end
end
player=object:extend()
function player:new(spawn_position)
    local position = spawn_position
    local draw_position = spawn_position - vector(4,4)
    local collision_check_position = draw_position - vector(4, 4)
    local direction = vector()
    local velocity = vector()
    local move_force = 5
    local mass = 1
    local max_acceleration = 0.2
    local max_speed = 2
    --local current_cell = nil
    local box_collider = box(0,0,8,8,-4,-4)
    -- interaction
    local interactables = {}
function self.position(v)
        if (v) position=v
        return position
end
    function self.update()
        -- convert player input into direction vector
        local dx = btn(0) and -1 or btn(1) and 1 or 0
        local dy = btn(2) and -1 or btn(3) and 1 or 0
        direction = vector(dx, dy)
        
        if #interactables > 0 and btnp(4) then
            interactables[1].interact()
return
        end
        -- calculate desired velocity and acceleration
        local desired_velocity = direction * vector(move_force, move_force)
        local acceleration = desired_velocity - velocity
        acceleration = acceleration.truncate(max_acceleration)
        acceleration /= vector(mass, mass)
        -- apply acceleration and cap at max speed
        velocity += acceleration
        velocity = velocity.truncate(max_speed)
        -- check for collisions/triggers
        collision_partition.act(collision_check_position.x(),collision_check_position.y(),
            function(obj)
                if obj~=box_collider then
                    local hit_x = box_collider.check(obj, velocity.x(), 0)
                    local hit_y = box_collider.check(obj, 0, velocity.y())
                    if (hit_x) velocity.x(0)
                    if (hit_y) velocity.y(0)
                end
            end)
        
        interactables = {}
trigger_partition.act(collision_check_position.x(),collision_check_position.y(),
            function(obj)
local hit = box_collider.check(obj, velocity.x(), velocity.y())
if hit then add(interactables, obj) end
end)
        -- apply velocity to position and update collider
        position += velocity
        draw_position = position - vector(4,4)
        collision_check_position = draw_position - vector(4,4)
        box_collider.update(position.x(), position.y())
        --current_cell = collision_partition.place(position.x(), position.y(), box_collider, current_cell)
    end
    function self.draw()
        spr(167, draw_position.x(), draw_position.y())
    end
end
function normal_from_value(a_val,a_min,a_max)
return (a_val-a_min)/(a_max-a_min)
end
function value_from_normal(a_nor,a_min,a_max)
return a_nor*(a_max-a_min)+a_min
end
function clamp(a_val,a_min,a_max)
if a_val<a_min then
return a_min
elseif a_val>a_max then
return a_max
else
return a_val
end
end
function yield_amount(a_a)
for i=1,a_a do
yield()
end
end
box=object:extend()
-- na_x = x position of top-left
-- na_y = y position of top-left
-- na_w = width of box
-- na_h = height of box
-- na_ox = offset x of box position
-- na_oy = offset y of box position
function box:new(na_x,na_y,na_w,na_h,na_ox,na_oy)
-- vars
local m_tl, m_br, m_o, m_s = nil, nil, vector(na_ox,na_oy), vector(na_w,na_h)
-- properties
function self.tl()
return m_tl
end
function self.br()
return m_br
end
-- functions
function self.update(a_x,a_y)
m_tl=vector(a_x+m_o.x(), a_y+m_o.y())
m_br=vector(m_tl.x()+m_s.x(), m_tl.y()+m_s.y())
end
-- a_o = the other box you're checking collision against
-- a_xv = x velocity
-- a_yv = y velocity
function self.check(a_o,a_xv,a_yv)
local l_otl, l_obr = a_o.tl(), a_o.br()
if not (m_tl.x()+a_xv>l_obr.x() or m_br.x()+a_xv<l_otl.x()) and not (m_tl.y()+a_yv>l_obr.y() or m_br.y()+a_yv<l_otl.y()) then
return true
else
return false
end
end
function self.draw(c)
rect(m_tl.x(), m_tl.y(), m_br.x(), m_br.y(), c)
end
-- init
self.update(na_x,na_y)
end
coroutine=object:extend()
function coroutine:new()
-- vars
local m_co = {}
-- functions
function self.tick()
for c in all(m_co) do
if costatus(c) then
coresume(c)
else
del(m_co,c)
end
end
end
function self.add(a_f)
add(m_co,cocreate(a_f))
end
end
partition=object:extend()
function partition:new(na_w,na_h,na_sx,na_sy)
-- vars
local m_sx,
m_sy,
m_dvx,
m_dvy,
m_cel=
na_sx,
na_sy,
na_w/na_sx,
na_h/na_sy,
{}
-- functions
function self.place(a_x,a_y,a_o,a_pc)
self.remove(a_o,a_pc)
local l_cel = m_cel[ceil(a_x/m_dvx)][ceil(a_y/m_dvy)]
add(l_cel,a_o)
return l_cel
end
function self.remove(a_o,a_pc)
if a_pc~=nil then
del(a_pc, a_o)
else
for k,v in pairs(m_cel) do
for k2,v2 in pairs(v) do
for k3,v3 in pairs(v2) do
if v3==a_o then
del(v2,a_o)
break
end
end
end
end
end
end
function self.get_cells(a_x,a_y)
local l_csl={}
for cx=-1,1 do
for cy=-1,1 do
local l_cx, l_cy = ceil(a_x/m_dvx) + cx, ceil(a_y/m_dvy) + cy
if l_cx > -1 and l_cx < (m_sx+1) and l_cy > -1 and l_cy < (m_sy+1) then
add(l_csl, m_cel[l_cx][l_cy])
end
end
end
return l_csl
end
function self.act(a_x,a_y,a_a)
local l_cel, l_l = self.get_cells(a_x,a_y), {}
for k,v in pairs(l_cel) do
for k2,v2 in pairs(v) do
add(l_l,{obj=v2})
end
end
for func in all(l_l) do
a_a(func.obj)
end
end
function self.draw(c)
for k,v in pairs(m_cel) do
for k2,v2 in pairs(v) do
for k3,v3 in pairs(v2) do
v3.draw(c)
end
end
end
end
-- init
for x=0,m_sx do
m_cel[x]={}
for y=0,m_sy do
m_cel[x][y]={}
end
end
end
text=object:extend()
function text:new(na_t,na_x,na_y)
-- vars
local m_t, m_x, m_y = na_t, na_x, na_y
-- properties
function self.x(v)
if (v) m_x=v
return m_x
end
function self.y(v)
if (v) m_y=v
return m_y
end
-- functions
function self.draw(a_c,a_o,a_oc)
if a_o then
for tx=-1,1 do
for ty=-1,1 do
print(m_t,m_x+tx,m_y+ty,a_oc)
end
end
end
print(m_t,m_x,m_y,a_c)
end
end
vector=object:extend()
-- operators
-- addition
function vector:__add(a_o)
return vector(self.x()+a_o.x(), self.y()+a_o.y())
end
-- multiplication
function vector:__mul(a_o)
return vector(self.x()*a_o.x(), self.y()*a_o.y())
end
-- subtraction
function vector:__sub(a_o)
return vector(self.x()-a_o.x(), self.y()-a_o.y())
end
-- division
function vector:__div(a_o)
return vector(self.x()/a_o.x(), self.y()/a_o.y())
end
-- magnitude
 function vector:__len()
 local l_ax, l_ay = abs(self.x()), abs(self.y())
 local l_d = max(l_ax,l_ay)
 local l_n = min(l_ax,l_ay) / l_d
 return sqrt(l_n*l_n + 1) * l_d
 end
-- normalize
function vector:__unm()
local l_l = #self
if l_l<=0 then
return vector()
else
return vector(self.x()/l_l, self.y()/l_l)
end
end
-- ctor
function vector:new(na_x,na_y)
-- vars
local m_x, m_y = na_x or 0, na_y or 0
-- properties
function self.x(v)
if (v) m_x=v
return m_x
end
function self.y(v)
if (v) m_y=v
return m_y
end
-- functions
function self.clone()
return vector(m_x,m_y)
end
function self.truncate(a_m)
if #self > a_m then
return -self * vector(a_m,a_m)
end
return self
end
end
