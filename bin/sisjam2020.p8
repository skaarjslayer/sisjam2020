pico-8 cartridge // http://www.pico-8.com
version 8
__lua__
local object={}
object.__index=object
function object:extend()
local new={}
for k, v in pairs(self) do
if sub(k,1,2)=="__" then
new[k] = v
end
end
new.__index = new
setmetatable(new, self)
return new
end
function object:__call(...)
local obj = setmetatable({}, self)
obj:new(...)
return obj
end
function object:merge(table)
for k,v in pairs(table) do
self[k] = v
end
end
state_machine=object:extend()
function state_machine:new()
-- vars
local m_sts, m_cst = {}, nil
-- properties
function self.current_state()
return m_cst
end
-- functions
function self.update()
if m_cst then
if m_cst.update then
m_cst.update()
end
end
end
function self.draw()
if m_cst then
if m_cst.draw then
m_cst.draw()
end
end
end
function self.push(a_nst)
if m_cst then
if m_cst.disable then
m_cst.disable()
end
end
m_sts[#m_sts + 1]=a_nst
m_cst=a_nst
if m_cst.enter then
m_cst.enter()
end
if m_cst.enable then
m_cst.enable()
end
end
function self.advance(a_nst)
if m_cst then
if m_cst.disable then
m_cst.disable()
end
if m_cst.exit then
m_cst.exit()
end
del(m_sts, m_cst)
end
m_sts[#m_sts + 1]=a_nst
m_cst=a_nst
if m_cst.enter then
m_cst.enter()
end
if m_cst.enable then
m_cst.enable()
end
end
end
debug_collision = false
collision_partition = nil
trigger_partition = nil
player_ref=nil
camera_ref=nil
levels={}
level=nil
next_level=nil
spawn_pos=nil
text_boxes={}
function _init()
    setup_level_data()
    next_level = levels[2]
    spawn_pos=vector(48,64)
end
function _update()
    if next_level then
        level = next_level
        level.init()
        next_level = nil
        player_ref=player(spawn_pos)
        camera_ref=player_camera(spawn_pos)
    end
    
    if #text_boxes > 0 then
        -- Text box will block other operations
        if btnp(4) then deli(text_boxes,1) end
    else
        player_ref.update()
        camera_ref.update()
    end
end
function _draw()
    camera_ref.draw()
    
    cls()
    palt(14, true)
    
    level.draw()
    palt(14, true)
    
    player_ref.draw()
    
    level.overdraw()
    
    if debug_collision then
        collision_partition.draw(3)
        trigger_partition.draw(4)
    end
    
    if #text_boxes > 0 then
        camera()
        draw_text_box(text_boxes[1],1,12)
    end
end
function setup_level_data()
    levels[1]=level(0,0,0,0,16,16)
    levels[1].add_interactable(48,16,interactable(box(48,16,8,8),load_level,{num=2,x=48,y=32}))
    levels[1].add_interactable(80,72,interactable(box(80,72,8,8),show_text_boxes,{"this is a chest.", "duh!"}))
    levels[2]=level(16,0,0,0,12,17)
    levels[2].add_interactable(48,24,interactable(box(48,24,8,8),load_level,{num=1,x=48,y=32}))
    return
end
function load_level(lvl_dat)
    next_level = levels[lvl_dat.num]
    spawn_pos = vector(lvl_dat.x,lvl_dat.y)
    level.init()
end
function show_text_boxes(tbs)
    text_boxes={}
    for i,v in ipairs(tbs) do add(text_boxes, v) end
end
function draw_text_box(text, box_c, text_c)
    --wdith 96, height 24
    rectfill(16,52,112,76,box_c)
    print(text,24,60,text_c)
end
interactable=object:extend()
function interactable:new(p_box, p_action, p_param)
    local m_box, m_action, m_param = p_box, p_action, p_param
    function self.interact()
        m_action(m_param)
    end
    -- Passtrhrough call to box
    function self.tl()
        return m_box.tl()
    end
    
    -- Passtrhrough call to box
    function self.br()
        return m_box.br()
    end
    -- Passtrhrough call to box
    function self.update(a_x,a_y)
        m_box.update(a_x,a_y)
end
    
    -- Passtrhrough call to box
    function self.check(a_o,a_xv,a_yv)
        m_box.check(a_o,a_xv,a_yv)
    end
    -- Passtrhrough call to box
    function self.draw(c)
        m_box.draw(c)
    end
end
level=object:extend()
function level:new(na_cx, na_cy, na_sx, na_sy, na_w, na_h)
    local cellx = na_cx
    local celly = na_cy
    local screenx = na_sx
    local screeny = na_sy
    local width = na_w
    local height = na_h
    local m_collision_partition=partition(na_w*8,na_h*8,na_w,na_h)
    local m_trigger_partition=partition(na_w*8,na_h*8,na_w,na_h)
    local player_spawn_position = vector()
    --properties
    function self.player_spawn_position(v)
        if (v) player_spawn_position=v
        return player_spawn_position
    end
    for x=0,na_w-1 do
        for y=0,na_h-1 do
            local sprite = mget(na_cx+x,na_cy+y)
            local collision_flag = fget(sprite, 1)
            local nx=x*8
            local ny=y*8
            if collision_flag then
                m_collision_partition.place(nx, ny, box(nx,ny,8,8))
            end
            if sprite==185 then
                player_spawn_position = vector(nx,ny)
            end
        end
    end
    function self.add_interactable(posx, posy, inter)
        m_trigger_partition.place(posx, posy, inter)
    end
    function self.init()
        collision_partition = m_collision_partition
        trigger_partition = m_trigger_partition
    end
    function self.draw()
        map(cellx, celly, screenx, screeny, width, height, 0x1)
        pal(2,1)
        map(cellx, celly, screenx, screeny, width, height, 0x80)
        pal()
    end
    function self.overdraw()
        map(cellx, celly, screenx, screeny, width, height, 0x4)
    end
end
player_camera=object:extend()
function player_camera:new(pos)
    local position=pos-vector(64, 64)
    local velocity=vector()
    local direction=vector()
    local target_position=vector()
    local move_force = 5
    local mass = 1
    local max_acceleration = 0.2
    local max_speed = 2
    
    function self.update()
        target_position = player_ref.position() - vector(64, 64)
        local diff_vector = target_position - position
        local direction = -diff_vector
        local desired_velocity = direction * vector(move_force, move_force)
        local acceleration = desired_velocity - velocity
        acceleration = acceleration.truncate(max_acceleration)
        acceleration /= vector(mass, mass)
        -- apply acceleration and cap at max speed
        velocity += acceleration
        velocity = velocity.truncate(max_speed)
        if #diff_vector<16 then
            velocity = vector()
        end
        position += velocity
    end
    function self.draw()
        camera(position.x(),position.y())  
    end
end
player=object:extend()
function player:new(spawn_position)
    local position = spawn_position
    local draw_position = spawn_position - vector(4,4)
    local collision_check_position = draw_position - vector(4, 4)
    local direction = vector()
    local velocity = vector()
    local move_force = 5
    local mass = 1
    local max_acceleration = 0.2
    local max_speed = 2
    --local current_cell = nil
    local box_collider = box(0,0,8,8,-4,-4)
    -- interaction
    local interactables = {}
function self.position(v)
        if (v) position=v
        return position
end
    function self.update()
        -- convert player input into direction vector
        local dx = btn(0) and -1 or btn(1) and 1 or 0
        local dy = btn(2) and -1 or btn(3) and 1 or 0
        direction = vector(dx, dy)
        
        if #interactables > 0 and btnp(4) then
            interactables[1].interact()
return
        end
        -- calculate desired velocity and acceleration
        local desired_velocity = direction * vector(move_force, move_force)
        local acceleration = desired_velocity - velocity
        acceleration = acceleration.truncate(max_acceleration)
        acceleration /= vector(mass, mass)
        -- apply acceleration and cap at max speed
        velocity += acceleration
        velocity = velocity.truncate(max_speed)
        -- check for collisions/triggers
        collision_partition.act(collision_check_position.x(),collision_check_position.y(),
            function(obj)
                if obj~=box_collider then
                    local hit_x = box_collider.check(obj, velocity.x(), 0)
                    local hit_y = box_collider.check(obj, 0, velocity.y())
                    if (hit_x) velocity.x(0)
                    if (hit_y) velocity.y(0)
                end
            end)
        
        interactables = {}
trigger_partition.act(collision_check_position.x(),collision_check_position.y(),
            function(obj)
local hit = box_collider.check(obj, velocity.x(), velocity.y())
if hit then add(interactables, obj) end
end)
        -- apply velocity to position and update collider
        position += velocity
        draw_position = position - vector(4,4)
        collision_check_position = draw_position - vector(4,4)
        box_collider.update(position.x(), position.y())
        --current_cell = collision_partition.place(position.x(), position.y(), box_collider, current_cell)
    end
    function self.draw()
        spr(167, draw_position.x(), draw_position.y())
    end
end
function normal_from_value(a_val,a_min,a_max)
return (a_val-a_min)/(a_max-a_min)
end
function value_from_normal(a_nor,a_min,a_max)
return a_nor*(a_max-a_min)+a_min
end
function clamp(a_val,a_min,a_max)
if a_val<a_min then
return a_min
elseif a_val>a_max then
return a_max
else
return a_val
end
end
function yield_amount(a_a)
for i=1,a_a do
yield()
end
end
box=object:extend()
-- na_x = x position of top-left
-- na_y = y position of top-left
-- na_w = width of box
-- na_h = height of box
-- na_ox = offset x of box position
-- na_oy = offset y of box position
function box:new(na_x,na_y,na_w,na_h,na_ox,na_oy)
-- vars
local m_tl, m_br, m_o, m_s = nil, nil, vector(na_ox,na_oy), vector(na_w,na_h)
-- properties
function self.tl()
return m_tl
end
function self.br()
return m_br
end
-- functions
function self.update(a_x,a_y)
m_tl=vector(a_x+m_o.x(), a_y+m_o.y())
m_br=vector(m_tl.x()+m_s.x(), m_tl.y()+m_s.y())
end
-- a_o = the other box you're checking collision against
-- a_xv = x velocity
-- a_yv = y velocity
function self.check(a_o,a_xv,a_yv)
local l_otl, l_obr = a_o.tl(), a_o.br()
if not (m_tl.x()+a_xv>l_obr.x() or m_br.x()+a_xv<l_otl.x()) and not (m_tl.y()+a_yv>l_obr.y() or m_br.y()+a_yv<l_otl.y()) then
return true
else
return false
end
end
function self.draw(c)
rect(m_tl.x(), m_tl.y(), m_br.x(), m_br.y(), c)
end
-- init
self.update(na_x,na_y)
end
coroutine=object:extend()
function coroutine:new()
-- vars
local m_co = {}
-- functions
function self.tick()
for c in all(m_co) do
if costatus(c) then
coresume(c)
else
del(m_co,c)
end
end
end
function self.add(a_f)
add(m_co,cocreate(a_f))
end
end
partition=object:extend()
function partition:new(na_w,na_h,na_sx,na_sy)
-- vars
local m_sx,
m_sy,
m_dvx,
m_dvy,
m_cel=
na_sx,
na_sy,
na_w/na_sx,
na_h/na_sy,
{}
-- functions
function self.place(a_x,a_y,a_o,a_pc)
self.remove(a_o,a_pc)
local l_cel = m_cel[ceil(a_x/m_dvx)][ceil(a_y/m_dvy)]
add(l_cel,a_o)
return l_cel
end
function self.remove(a_o,a_pc)
if a_pc~=nil then
del(a_pc, a_o)
else
for k,v in pairs(m_cel) do
for k2,v2 in pairs(v) do
for k3,v3 in pairs(v2) do
if v3==a_o then
del(v2,a_o)
break
end
end
end
end
end
end
function self.get_cells(a_x,a_y)
local l_csl={}
for cx=-1,1 do
for cy=-1,1 do
local l_cx, l_cy = ceil(a_x/m_dvx) + cx, ceil(a_y/m_dvy) + cy
if l_cx > -1 and l_cx < (m_sx+1) and l_cy > -1 and l_cy < (m_sy+1) then
add(l_csl, m_cel[l_cx][l_cy])
end
end
end
return l_csl
end
function self.act(a_x,a_y,a_a)
local l_cel, l_l = self.get_cells(a_x,a_y), {}
for k,v in pairs(l_cel) do
for k2,v2 in pairs(v) do
add(l_l,{obj=v2})
end
end
for func in all(l_l) do
a_a(func.obj)
end
end
function self.draw(c)
for k,v in pairs(m_cel) do
for k2,v2 in pairs(v) do
for k3,v3 in pairs(v2) do
v3.draw(c)
end
end
end
end
-- init
for x=0,m_sx do
m_cel[x]={}
for y=0,m_sy do
m_cel[x][y]={}
end
end
end
text=object:extend()
function text:new(na_t,na_x,na_y)
-- vars
local m_t, m_x, m_y = na_t, na_x, na_y
-- properties
function self.x(v)
if (v) m_x=v
return m_x
end
function self.y(v)
if (v) m_y=v
return m_y
end
-- functions
function self.draw(a_c,a_o,a_oc)
if a_o then
for tx=-1,1 do
for ty=-1,1 do
print(m_t,m_x+tx,m_y+ty,a_oc)
end
end
end
print(m_t,m_x,m_y,a_c)
end
end
vector=object:extend()
-- operators
-- addition
function vector:__add(a_o)
return vector(self.x()+a_o.x(), self.y()+a_o.y())
end
-- multiplication
function vector:__mul(a_o)
return vector(self.x()*a_o.x(), self.y()*a_o.y())
end
-- subtraction
function vector:__sub(a_o)
return vector(self.x()-a_o.x(), self.y()-a_o.y())
end
-- division
function vector:__div(a_o)
return vector(self.x()/a_o.x(), self.y()/a_o.y())
end
-- magnitude
 function vector:__len()
 local l_ax, l_ay = abs(self.x()), abs(self.y())
 local l_d = max(l_ax,l_ay)
 local l_n = min(l_ax,l_ay) / l_d
 return sqrt(l_n*l_n + 1) * l_d
 end
-- normalize
function vector:__unm()
local l_l = #self
if l_l<=0 then
return vector()
else
return vector(self.x()/l_l, self.y()/l_l)
end
end
-- ctor
function vector:new(na_x,na_y)
-- vars
local m_x, m_y = na_x or 0, na_y or 0
-- properties
function self.x(v)
if (v) m_x=v
return m_x
end
function self.y(v)
if (v) m_y=v
return m_y
end
-- functions
function self.clone()
return vector(m_x,m_y)
end
function self.truncate(a_m)
if #self > a_m then
return -self * vector(a_m,a_m)
end
return self
end
end
__gfx__
bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbeeeeeeeeeeeeeeeebbbbbbbbbbbbbbbbbbbbbbbbeeeeeeeeeeeeeeeeeee200eeeee00eee
bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb33b3b3b33bb3b3b33bbbbeeeeeeeeeeeeeeeebbb33b3b3b33bb3b3b33bbbbeeeeeeeeeeeeeeeeee282d0ee002200e
bbbbbbbbbbbbbb3bbbbbbbbbbbbbbbbbbbbee3e3e3ee33e3e3ee3bbbeeeeeeeeeeeeeeeebbb223232322332323223bbbeeeeeeeeeeeeeeeee288210e02000020
bbbbbbbbbbbbb33bbbbbbbbbbb3bbbbbbb3eeeeeeeeeeeeeeeeee3bbeeeeeeeeeeeeeeeebb32442224424222244223bbeeeeeeeeeeeeeeee2888222001022010
bbbbbbbb33bbb3bbbbbbbbbbbb33bbbbb3eeeeeeeeeeeeeeeeeeee3beeeeeeeeeeeeeeeeb3224424244222422442423beeeeeeeeeeeeeeee2882022001111110
bbbbbbbbb33bbbbbbbbbb33bbbb3bbbbbeeeeeeeeeeeeeeeeeeeeebbeeeeeeeeeeeeeeeeb242222222222222222222bbeeeeeeeeeeeeeeee2824402000111100
bbbbbbbbbbbbbbbbbbbb33bbbbbbbbbbbbeeeeeeeeeeeeeeeeeeee3beeeeeeeeeeeeeeeebb22dddddddddddddddd243beeeeeeeeeeeeeeee2242440001000010
bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb3eeeeeeeeeeeeeeeeeeeeebeeeeeeeeeeeeeeeeb32deeeeeeeeeeeeeeeed22beeeeeeddddeeeeeee000000e01111010
33333333333333333333333333333333bbeeeeeeeeeeeeeeeeeeeebbeeeeeee33eeeeeeebbdeeeeeeeeeeeeeeeeeedbbeeeeed2332deeeeeeeeeeeee00111000
33333333333333333333333333333333beeeeeeeeeeeeeeeeeeeeeebeeeeeeeeeeeeeeeeb3deeeeeeeeeeeeeeeeeed3beeeeed2222deeeeeeeeeeeee01000010
33333333333333b33333333333333333bbeeeeeeeeeeeeeeeeeeeeebeeeeeeeeeeeeeeeebbdeeeeeeeeeeeeeeeeeed2beeeeed2222deeeeeeeeeeeee01011110
3333333333333bb33333333333b33333bbeeeeeeeeeeeeeeeeeeeebbeeeeeeeeeeeeeeeebbdeeeeeeeeeeeeeeeeeedbbeeeeed2222deeeeeeeeeeeee00011100
33333333bb333b333333333333bb3333beeeeeeeeeeeeeeeeeeeeebbeeeeeeeeeeeeeeeeb3deeeeeeeeeeeeeeeeeedbbeeeeeed22deeeeeeeeeeeeee01000010
333333333bb3333333333bb3333b3333beeeeeeeeeeeeeeeeeeeeeebeeeeeeeeeeeeeeeeb2deeeeeeeeeeeeeeeeeed3beeeeeeeddeeeeeeeeeeeeeee01111010
33333333333333333333bb3333333333bbeeeeeeeeeeeeeeeeeeeebbeeeeeeeeeeeeeeeebbdeeeeeeeeeeeeeeeeeedbbeeeeeeeeeeeeeeeeeeeeeeeee011100e
33333333333333333333333333333333beeeeeeeeeeeeeeeeeeeeeebeeeeeeeeeeeeeeeeb3deeeeeeeeeeeeeeeeeed3beeeeeeeeeeeeeeeeeeeeeeeeee0000ee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeebbeeeeeeeeeeeeeeeeeeeebbeeeeeeeeeeeeeeeebbdeeeeeeeeeeeeeeeeeedbb32deeeeeeeeeed23eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeebeeeeeeeeeeeeeeeeeeeeeebeeeeeeeeeeeeeeeeb3deeeeeeeeeeeeeeeeeed3b22deeeeeeeeeed22eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeebbeeeeeeeeeeeeeeeeeeeeebeeeeeeeeeeeeeeeebbdeeeeeeeeeeeeeeeeeed2b22deeeeeeeeeed22eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeebbeeeeeeeeeeeeeeeeeeeebbeeeeeeeeeeeeeeeebbdeeeeeeeeeeeeeeeeeedbb22deeeeeeeeeed22eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeebeeeeeeeeeeeeeeeeeeeeebbeeeeeeeeeeeeeeeeb3deeeeeeeeeeeeeeeeeedbb2deeeeeeeeeeeed2eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeebbbeeeeeeeeeeeeeeeeeebbbeeeeeeeeeeeeeeeebbbeeeeeeeeeeeeeeeeeebbbdeeeeeeeeeeeeeedeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeebbbbeebebebbeebebebbebbbeeeeeeeeeeeeeeeebbbbeebebebbeebebebbebbbeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeebbbbbbbbbbbbbbbbbbbbbbbbeeeeeeeeeeeeeeeebbbbbbbbbbbbbbbbbbbbbbbbeeeeeeddddeeeeeeeeeeeeeeeeeeeeee
22222222222222212222222222222222222222222222222222222222eeeeeeeeeeeeeeee222222222222222222222222eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
22222222222222222112222222222222222212211122122111221222eeeeeeeeeeeeeeee222212211122122111221222eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
222222222222222221122222222222222221e11eee11e11eee11e222eeeeeeeeeeeeeeee222111111111111111111222eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
22222222221222222222222222222222211eeeeeeeee2eeeeeeee112eeeeeeeeeeeeeeee211122111221211112211112eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
2222222222222222222222222122222222eeeeeeeeee1eeeeeeeeee2eeeeeeeeeeeeeeee221122121221112112212112eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
2222222222221122222222222222222222eeeeeeeeeeeeeeeeeeee22eeeeeeeeeeeeeeee222111111111111111111122eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
2222222222221122222222222222212221eeeeeeeeeeeeeeeeeeee22eeeeeeeeeeeeeeee2111dddddddddddddddd1222eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
222222222222222222222222222222222eeeeeeeeeeeeeeeeeeeee12eeeeeeeeeeeeeeee211deeeeeeeeeeeeeeeed112eeeeeeddddeeeeeeeeeeeeeeeeeeeeee
1111111111111112111111111111111122eeeeeeeeeeeeeeeeeeee22eeeeeee11eeeeeee22deeeeeeeeeeeeeeeeeed22eeeeed1111deeeeeeeeeeeeeeeeeeeee
1111111111111111122111111111111122eeeeeeeeeeeeeeeeeeee22eeeeeeeeeeeeeeee22deeeeeeeeeeeeeeeeeed22eeeeed1111deeeeeeeeeeeeeeeeeeeee
1111111111111111122111111111111121e2eeeeeeeeeeeeeeeeee12eeeeeeeeeeeeeeee21deeeeeeeeeeeeeeeeeed12eeeeed1111deeeeeeeeeeeeeeeeeeeee
111111111121111111111111111111112ee1eeeeeeeeeeeeeeeeeee2eeeeeeeeeeeeeeee21deeeeeeeeeeeeeeeeeed12eeeeed1111deeeeeeeeeeeeeeeeeeeee
1111111111111111111111111211111122eeeeeeeeeeeeeeeeee2ee2eeeeeeeeeeeeeeee22deeeeeeeeeeeeeeeeeed12eeeeeed11deeeeeeeeeeeeeeeeeeeeee
1111111111112211111111111111111122eeeeeeeeeeeeeeeeee1e22eeeeeeeeeeeeeeee22deeeeeeeeeeeeeeeeeed22eeeeeeeddeeeeeeeeeeeeeeeeeeeeeee
1111111111112211111111111111121121eeeeeeeeeeeeeeeeeeee22eeeeeeeeeeeeeeee21deeeeeeeeeeeeeeeeeed22eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
111111111111111111111111111111112eeeeeeeeeeeeeeeeeeeee12eeeeeeeeeeeeeeee21deeeeeeeeeeeeeeeeeed12eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee22eeeeeeeeeeeeeeeeeeee22eeeeeeeeeeeeeeee22deeeeeeeeeeeeeeeeeed2211deeeeeeeeeed11eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee22eeeeeeeeeeeeeeeeeeee22eeeeeeeeeeeeeeee22deeeeeeeeeeeeeeeeeed2211deeeeeeeeeed11eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee21eeeeeeeeeeeeeeeeeeee12eeeeeeeeeeeeeeee21deeeeeeeeeeeeeeeeeed1211deeeeeeeeeed11eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee2eeeeeeeeeeeeeeeeeeeeee2eeeeeeeeeeeeeeee21deeeeeeeeeeeeeeeeeed1211deeeeeeeeeed11eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee22eeeeeeeeeeee2eeeeeeee2eeeeeeeeeeeeeeee22deeeeeeeeeeeeeeeeeed121deeeeeeeeeeeed1eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee222eeeeeeeeeee1eeeeee222eeeeeeeeeeeeeeee222eeeeeeeeeeeeeeeeee222deeeeeeeeeeeeeedeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee222e22eee22e22eee22e2222eeeeeeeeeeeeeeee222e22eee22e22eee22e2222eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee222222222222222222222222eeeeeeeeeeeeeeee222222222222222222222222eeeeeeddddeeeeeeeeeeeeeeeeeeeeee
e3333eeee3333eeee3333eeee3333eeee333333bb3333eee2222222211100111011001110110011011100110111111100111111001111111eeeeeeeeeeeeeeee
3bbbb3ee3bbbb3ee3bbbb3ee3bbbb3ee3bbbb3333bbbb3ee2222222211100111011001110110011011100110111111100111111001111111eeeeeeeeeeeeeeee
bbbbbb33bbbbbb33bbbbbb33bbbbbb33bbbbbb33bbbbbb3e2222222211100111011001110110011011100110110000000000000000000011eeeeeeeeeeeeeeee
bbbbbb33bbbbbb33b3bbbb33b3bbbb33b3bbbb33b3bbbb3e2222222200000000000000000000000000000000110000000000000000000011eeeeeeeeeeeeeeee
b33bb333b33bb333333bb333333bb333333bb333333bb33e222222220111111001111110011111100111111011dddddddddddddddddddd11eeeeeeeeeeeeeeee
333333ee33333bbbb3333bbbb33333ebb3333bbbb3333bbe222222220111111001111110011111100111111011deeeeeeeeeeeeeeeeeed11eeeeeeeeeeeeeeee
e3223eeee323bbbbbb33bbbbbb323eebbb33bbbbbb33bbbe222222220111111001111110011111100111111011deeeeeeeeeeeeeeeeeed11eeeeeeeeeeeeeeee
ee22eeeeee23b3bbbb33b3bbbb32eeebbb33b3bbbb33b3be222222220000000000000000000000000000000000deeeeeeeeeeeeeeeeeed00eeeeeed00deeeeee
eeeeeeeee333333bb333333bb3333eebb333333bb333333e111111111100001101000011010000101100001000deeeeeeeeeeeeeeeeeed00eeeeed0000deeeee
eeeeeeee3bbbb3333bbbb3333bbbb3e33bbbb3333bbbb33e111111111100011101000111010001101100011011deeeeeeeeeeeeeeeeeed11eeeeeed00deeeeee
eeeeeee3bbbbbb33bbbbbb33bbbbbb33bbbbbb33bbbbbb3e111111111000011100000111000001101000011011deeeeeeeeeeeeeeeeeed11eeeeeeeddeeeeeee
eeeeeee3bbbbbb33b3bbbb33b3bbbb33bbbbbb33b3bbbb3e111111110000000000000000000000000000000011deeeeeeeeeeeeeeeeeed11eeeeeeeddeeeeeee
eeeeeee3b33bb333333bb333333bb333b33bb333333bb33e111111110000000000000000000000000000000011deeeeeeeeeeeeeeeeeed11eeeeeeeeeeeeeeee
eeeeeeee33333bbbb3333bbbb33333ee33333bbbb33333ee111111110000011000000110000001100000011011deeeeeeeeeeeeeeeeeed11eeeeeeeeeeeeeeee
eeeeeeeee323bbbbbb33bbbbbb323eeee323bbbbbb323eee111111110000111000001110000011100000111011deeeeeeeeeeeeeeeeeed11eeeeeeeeeeeeeeee
eeeeeeeeee23b3bbbb33b3bbbb32eeeeee23b3bbbb32eeee111111110000000000000000000000000000000000deeeeeeeeeeeeeeeeeed00eeeeeeeeeeeeeeee
eeeeeeeee333333bb333333bb3333eeee333333bb3333eee000000001110011101100111011001101110011000deeeeeeeeeeeeeeeeeed0000deeeeeeeeeed00
eeeeeeee3bbbb3333bbbb3333bbbb3ee3bbbb3333bbbb3ee000000001110011101100111011001101110011011deeeeeeeeeeeeeeeeeed110deeeeeeeeeeeed0
eeeeeee3bbbbbb33bbbbbb33bbbbbb33bbbbbb33bbbbbb3e000000001100011101000111010001101100011011deeeeeeeeeeeeeeeeeed11deeeeeeeeeeeeeed
eeeeeee3bbbbbb33bbbbbb33bbbbbb33b3bbbb33b3bbbb3e000000000000000000000000000000000000000011deeeeeeeeeeeeeeeeeed11deeeeeeeeeeeeeed
eeeeeee3b33bb333b33bb333b33bb333333bb333333bb33e000000000110011001100110011001100110011011deeeeeeeeeeeeeeeeeed11eeeeeeeeeeeeeeee
eeeeeeee333333ee333333ee333333ebb33333ee33333bbe000000000111011001110110011101100111011011deeeeeeeeeeeeeeeeeed11eeeeeeeeeeeeeeee
eeeeeeeee3223eeee3223eeee3223eebbb323eeee323bbbe0000000001110010011100100111001001110010111111100111111001111111eeeeeeeeeeeeeeee
eeeeeeeeee22eeeeee22eeeeee22eeebbb32eeeeee23b3be0000000000000000000000000000000000000000111111100111111001111111eeeeeed00deeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee242444222eeee000000001110011101100111011001101110011000000000000000000000000000000000eeeeeeee
ee44eeeeee44eeeeee44eeeeee44eeeeee4422442244eeee000000001110011101100111011001101110011001101110000111100111111001111110eeeeeeee
e4444eeee4444eeee4444eeee4444eeee444422224444eee000000001110001101100011011000101110001001001110010111100111111001111110eeeeeeee
444444ee444444ee444444ee444444ee44444422444444ee000000000000000000000000000000000000000000000110011011000111111001111110eeeeeeee
44444244444442224244422242444242424442224244422e000000000001111000011110000111100001111000000000011100000111111001111110eeeeeeee
42444224424422442244224422444224224422442244224e000000000011111000111110001111100011111001100000011101100011111001111110eeeeeeee
224422ee2242244442222444422422e4422224444222244e000000000011111000111110001111100011111001110000011011100001111001111110eeeeeeee
eeeeeeeee22244444422444444222ee4442244444422444e000000000000000000000000000000000000000000000000000000000000000000000000eeeeeeee
eeeeeeeeee224244422242444222eee4422242444222424eee000eeeee000eeeeee000eeee000eeeeeeeeeee122222211222222112222221eeeeeeeeeeeeeeee
eeeeeeeeee442244224422442244eee4224422442244224ee00f0eeeee0f0eeeeee0f00eee050eeeeeeeeeee011111100111111001111110eeeeeeeeeeeeeeee
eeeeeeeee44442222444422224444ee2244442222444422e0f6560eee06560eeee0656f0e07660eeeeeeeeee010000100100001001000010eeeeeeeeeeeeeeee
eeeeeeee4444442244444422444444e2444444224444442e0f676f0e0f676f0ee0f676f0e06f60eeeeeeeeee010000100100001001000010eeeeeeeeeeeeeeee
eeeeeee4444442224244422242444244444442224244424e00166f0e0f666f0ee0f66100e06f60eeeeeeeeee010000100100001001011010eeeeeeeeeeeeeeee
eeeeeee4424422442244224422444224424422442244422ee0111f0e0f111f0ee0f1110eee0f10eeeeeeeeee010000100100001001000010eeeeeeeeeeeeeeee
eeeeeeee2242244442222444422422ee22422444422422eee01010eee01010eeee01010eee0110eeeeeeeeee010000100100001001022010eeeeeeeeeeeeeeee
eeeeeeeee22244444422444444222eeee222444444222eeeeeee10eee01010eeee01eeeeee010eeeeeeeeeee010000100100001001011010eeeeeeeeeeeeeeee
eeeeeeeeee224244422242444222eeeeee2242444222eeeeee000eeeee000eeeeee000ee00777700eeeeeeee112222111122221111222211eeeeeeeeeeeeeeee
eeeeeeeeee442244224422442244eeeeee4422442244eeeeee0500eeee050eeeee0050ee00700700eeeeeeee121111211211112112111121eeeeeeeeeeeeeeee
eeeeeeeee44442222444422224444eeee444422224444eeee0666f0ee06660eee0f6660e00700700eeeeeeee011001100110011001100110eeeeeeeeeeeeeeee
eeeeeeee4444442244444422444444ee44444422444444ee0f666f0e0f666f0ee0f666f000777700eeeeeeee010000100100001001000010eeeeeeeeeeeeeeee
eeeeeee4444442444444424444444244424442444444422e0f66600e0f666f0ee00666f000700000eeeeeeee010000100100001001011010eeeeeeeeeeeeeeee
eeeeeee4424442244244422442444224224442244244224e0f1110ee0f111f0eee0111f000700000eeeeeeee010000100100001001000010eeeeeeeeeeeeeeee
eeeeeeee224422ee224422ee224422e4422422ee2242244ee01010eee01010eeee01010e00700000eeeeeeee010000100100001001022010eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeee444222eeee222444ee01eeeeee01010eeeeeee10e00000000eeeeeeee010000100100001001011010eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee112222111122221111222211eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee122222211222222112222221eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee021001200210012002100120eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee010000100100001001000010eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee010000100100001001011010eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee010000100100001001000010eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee010000100100001001022010eeeeeeeeeeeeeeee
eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee010000100100001001011010eeeeeeeeeeeeeeee
111111111111111111111111eeeeeeeeeeeeeeee2111111c2111111d2111111d2111111ceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
111ccc1ccc11cc1ccc11c111eeeeeeeeeeeeeeee1211ddcc121111111211111112111dcceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
11ceecceecccecceecccec11eeeeeeeeeeeeeeee11dd111c11d11111111111111111111ceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
1ceeeceeeceeeceeeeceeec1eeeeeeeeeeeeeeeedd21112ddd211111111111111111112deeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
11ceeceeeeccceeeeeceeec1eeeeeeeeeeeeeeee2d1222d12d12211111111111111122d1eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
11ccceeeeeeeeeeeeeecccc1eeeeeeeeeeeeeeee1c111d111c1111111111111111111111eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
1cceeeeeeeeeeeeeeeeeec11eeeeeeecceeeeeee1ccdd2111cc111111111111111111211eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
1ceeeeeeeeeeeeeeeeeeeec1eeeeeec11ceeeeeec1111121c11111111111111111111121eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
1ceeeeeeeeeeeeeeeeeeeec1eeeeeec11ceeeeeeeeeeeeee21111111eeeeeeee1111111ceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
11ceeeeeeeeeeeeeeeeeecc1eeeeeeecceeeeeeeeeeeeeee12111111eeeeeeee11111dcceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
1ccceeeeeeeeeeeeeeeecc11eeeeeeeeeeeeeeeeeeeeeeee11d11111eeeeeeee1111111ceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
1ceeceeeeeeeeeeeeeecec11eeeeeeeeeeeeeeeeeeeeeeeedd211111eeeeeeee1111112deeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
11ceceeeeeeeeeeeeeeceec1eeeeeeeeeeeeeeeeeeeeeeee2d122111eeeeeeee111122d1eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
11cceeeeeeeeeeeeeeeeccc1eeeeeeeeeeeeeeeeeeeeeeee1c111111eeeeeeee11111111eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
1cceeeeeeeeeeeeeeeeeec11eeeeeeeeeeeeeeeeeeeeeeee1ccd1111eeeeeeee11111211eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
1ceeeeeeeeeeeeeeeeeeeec1eeeeeeeeeeeeeeeeeeeeeeeec1111111eeeeeeee11111121eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
1ceeeeeeeeeeeeeeeeeeeec11ceeeeeeeeeeeec1eeeeeeee21111111111111111111111ceeeeeeeeeeeeeeeeeeeeeeeee000000ee000000ee000000eeeeeeeee
11ceeeeeeeeeeeeeeeeeecc1ceeeeeeeeeeeeeeceeeeeeee121111111111111111111dcc0000000000000000000000000d1111d00611116009111190eeeeeeee
1cccceeeeeeeeeeeeeeccc11eeeeeeeeeeeeeeeeeeeeeeee11d1111d1111111d1111111c0dddddd0066666600999999006dddd60076666700adddda0eeeeeeee
1ceeeceeeeecceeeeeceec11eeeeeeeeeeeeeeeeeeeeeeeedd21112dd121112dd121112d06dddd6007dddd700adddda00d1111d00611116009111190eeeeeeee
1ceeeceeeeceeceeeeceeec1eeeeeeeeeeeeeeeeeeeeeeee2d1222d1211222d1211222d106666660077777700aaaaaa00dddddd00666666009999990eeeeeeee
11ceccceecceccceecceec11eeeeeeeeeeeeeeeeeeeeeeee1c111d111d111d111d111d11011dd1100116611001199110011dd1100116611001199110eeeeeeee
111c11ccc1cc11ccc1ccc111eeeeeeecceeeeeeeeeeeeeee1ccdd2111ccdd2111ccdd211011111100111111001111110011111100111111001111110eeeeeeee
111111111111111111111111eeeeeec11ceeeeeeeeeeeeeec1111121c1111121c1111121000000000000000000000000000000000000000000000000eeeeeeee

__gff__
0101010101010101010101010101030101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010103030303038203030303010101010101030303030304010101010101010101
0103030303030101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010001010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101
__map__
7676767676767676767676767676767661626263000000006162626263000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
766a69676967bb68686868766767677671727273000000007172727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
669678868686869696967a66000000667172727300000e007172727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
6689969678868696968696660000776671727273000000007172727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
669696969696b978969696660000006671727273000000007172727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
6696967896789696969696660000006671727273000000007172727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
6696969696969696968696660000006671727273000000007172727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
6676767676000076767676660000006671727273000000007172727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
6667676767000067676767670000006671727273000000007172727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
66000000000000000000fc000078006671727273000000007172727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
6600007800000000000000000000006671727273000000007172727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
6600000000000000000000000000006671727273000000007172727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
6600000000000077007800000000006671727273000000007172727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
6600000000000000000000000000006671727273000000007172727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
6676767676767676767676767676766671727273000000007172727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
6767676767676767676767676767676771727262626262626272727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000071727272727272727272727273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__sfx__
000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__music__
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344
00 41424344

