debug_collision = false

collision_partition = nil
trigger_partition = nil

player_ref=nil
camera_ref=nil

levels={}
level=nil
next_level=nil
spawn_pos=nil

text_boxes={}

function _init()
    setup_level_data()
    next_level = levels[2]
    spawn_pos=vector(48,64)
end

function _update()
    if next_level then
        level = next_level
        level.init()
        next_level = nil
        player_ref=player(spawn_pos)
        camera_ref=player_camera(spawn_pos)
    end
    
    if #text_boxes > 0 then
        -- Text box will block other operations
        if btnp(4) then deli(text_boxes,1) end
    else
        player_ref.update()
        camera_ref.update()
    end
end

function _draw()
    camera_ref.draw()
    
    cls()
    palt(14, true)
    
    level.draw()
    palt(14, true)
    
    player_ref.draw()
    
    level.overdraw()
    
    if debug_collision then
        collision_partition.draw(3)
        trigger_partition.draw(4)
    end
    
    if #text_boxes > 0 then
        camera()
        draw_text_box(text_boxes[1],1,12)
    end
end

function setup_level_data()
    levels[1]=level(0,0,0,0,16,16)
    levels[1].add_interactable(48,16,interactable(box(48,16,8,8),load_level,{num=2,x=48,y=32}))
    levels[1].add_interactable(80,72,interactable(box(80,72,8,8),show_text_boxes,{"this is a chest.", "duh!"}))

    levels[2]=level(16,0,0,0,12,17)
    levels[2].add_interactable(48,24,interactable(box(48,24,8,8),load_level,{num=1,x=48,y=32}))
    return
end

function load_level(lvl_dat)
    next_level = levels[lvl_dat.num]
    spawn_pos = vector(lvl_dat.x,lvl_dat.y)
    level.init()
end

function show_text_boxes(tbs)
    text_boxes={}
    for i,v in ipairs(tbs) do add(text_boxes, v) end
end

function draw_text_box(text, box_c, text_c)
    --wdith 96, height 24
    rectfill(16,52,112,76,box_c)
    print(text,24,60,text_c)
end