box=object:extend()

-- na_x = x position of top-left
-- na_y = y position of top-left
-- na_w = width of box
-- na_h = height of box
-- na_ox = offset x of box position
-- na_oy = offset y of box position
function box:new(na_x,na_y,na_w,na_h,na_ox,na_oy)

	-- vars
	local m_tl, m_br, m_o, m_s = nil, nil, vector(na_ox,na_oy), vector(na_w,na_h)

	-- properties
	function self.tl()
		return m_tl
	end

	function self.br()
		return m_br
	end

	-- functions
	function self.update(a_x,a_y)
		m_tl=vector(a_x+m_o.x(), a_y+m_o.y())
		m_br=vector(m_tl.x()+m_s.x(), m_tl.y()+m_s.y())
	end
	
	-- a_o = the other box you're checking collision against
	-- a_xv = x velocity
	-- a_yv = y velocity
	function self.check(a_o,a_xv,a_yv)
		local l_otl, l_obr = a_o.tl(), a_o.br()
		if not (m_tl.x()+a_xv>l_obr.x() or m_br.x()+a_xv<l_otl.x()) and not (m_tl.y()+a_yv>l_obr.y() or m_br.y()+a_yv<l_otl.y()) then
			return true
		else
			return false
		end
	end

	function self.draw(c)
		rect(m_tl.x(), m_tl.y(), m_br.x(), m_br.y(), c)
	end

	-- init
	self.update(na_x,na_y)
end