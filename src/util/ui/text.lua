text=object:extend()

function text:new(na_t,na_x,na_y)

	-- vars
	local m_t, m_x, m_y = na_t, na_x, na_y

	-- properties
	function self.x(v)
		if (v) m_x=v
		return m_x
	end

	function self.y(v)
		if (v) m_y=v
		return m_y
	end

	-- functions
	function self.draw(a_c,a_o,a_oc)
		if a_o then
			for tx=-1,1 do
				for ty=-1,1 do
					print(m_t,m_x+tx,m_y+ty,a_oc)
				end
			end
		end
	
		print(m_t,m_x,m_y,a_c)
	end
end