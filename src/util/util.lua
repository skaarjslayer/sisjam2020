function normal_from_value(a_val,a_min,a_max)
	return (a_val-a_min)/(a_max-a_min)
end

function value_from_normal(a_nor,a_min,a_max)
	return a_nor*(a_max-a_min)+a_min
end

function clamp(a_val,a_min,a_max)
	if a_val<a_min then
		return a_min
	elseif a_val>a_max then
		return a_max
	else
		return a_val
	end
end

function yield_amount(a_a)
	for i=1,a_a do
		yield()
	end
end