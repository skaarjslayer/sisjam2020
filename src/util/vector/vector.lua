vector=object:extend()

-- operators
-- addition
function vector:__add(a_o)
	return vector(self.x()+a_o.x(), self.y()+a_o.y())
end

-- multiplication
function vector:__mul(a_o)
	return vector(self.x()*a_o.x(), self.y()*a_o.y())
end

-- subtraction
function vector:__sub(a_o)
	return vector(self.x()-a_o.x(), self.y()-a_o.y())
end

-- division
function vector:__div(a_o)
	return vector(self.x()/a_o.x(), self.y()/a_o.y())
end

-- magnitude
 function vector:__len()
 	local l_ax, l_ay = abs(self.x()), abs(self.y())
 	local l_d = max(l_ax,l_ay)
 	local l_n = min(l_ax,l_ay) / l_d
 	return sqrt(l_n*l_n + 1) * l_d
 end

-- normalize
function vector:__unm()
	local l_l = #self
	if l_l<=0 then
		return vector()
	else
		return vector(self.x()/l_l, self.y()/l_l)
	end
end

-- ctor
function vector:new(na_x,na_y)
	
	-- vars
	local m_x, m_y = na_x or 0, na_y or 0

	-- properties
	function self.x(v)
		if (v) m_x=v
		return m_x
	end

	function self.y(v)
		if (v) m_y=v
		return m_y
	end

	-- functions
	function self.clone()
		return vector(m_x,m_y)
	end
	
	function self.truncate(a_m)
		if #self > a_m then
			return -self * vector(a_m,a_m)
		end
	
		return self
	end
end