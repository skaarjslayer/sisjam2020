partition=object:extend()

function partition:new(na_w,na_h,na_sx,na_sy)

	-- vars
	local m_sx,
			m_sy,
			m_dvx,
			m_dvy,
			m_cel	=
			
			na_sx,
			na_sy,
			na_w/na_sx,
			na_h/na_sy,
			{}

	-- functions
	function self.place(a_x,a_y,a_o,a_pc)
		self.remove(a_o,a_pc)
	
		local l_cel = m_cel[ceil(a_x/m_dvx)][ceil(a_y/m_dvy)]
	
		add(l_cel,a_o)
	
		return l_cel
	end
	
	function self.remove(a_o,a_pc)
		if a_pc~=nil then
			del(a_pc, a_o)
		else
			for k,v in pairs(m_cel) do
				for k2,v2 in pairs(v) do
					for k3,v3 in pairs(v2) do
						if v3==a_o then
							del(v2,a_o)
							break
						end
					end
				end
			end
		end
	end
	
	function self.get_cells(a_x,a_y)
		local l_csl={}
		for cx=-1,1 do
			for cy=-1,1 do
				local l_cx, l_cy = ceil(a_x/m_dvx) + cx, ceil(a_y/m_dvy) + cy

				if l_cx > -1 and l_cx < (m_sx+1) and l_cy > -1 and l_cy < (m_sy+1) then
					add(l_csl, m_cel[l_cx][l_cy])
				end
			end
		end
	
		return l_csl
	end
	
	function self.act(a_x,a_y,a_a)
		local l_cel, l_l = self.get_cells(a_x,a_y), {}

		for k,v in pairs(l_cel) do
			for k2,v2 in pairs(v) do
				add(l_l,{obj=v2})
			end
		end
	
		for func in all(l_l) do
			a_a(func.obj)
		end
	end

	function self.draw(c)
		for k,v in pairs(m_cel) do
			for k2,v2 in pairs(v) do
				for k3,v3 in pairs(v2) do
					v3.draw(c)
				end
			end
		end
	end

	-- init
	for x=0,m_sx do
		m_cel[x]={}
		for y=0,m_sy do
			m_cel[x][y]={}
		end
	end
end