coroutine=object:extend()

function coroutine:new()

	-- vars
	local m_co = {}

	-- functions
	function self.tick()
		for c in all(m_co) do
			if costatus(c) then
				coresume(c)
			else
				del(m_co,c)
			end
		end
	end
	
	function self.add(a_f)
		add(m_co,cocreate(a_f))
	end
end