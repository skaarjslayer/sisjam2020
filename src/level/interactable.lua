interactable=object:extend()

function interactable:new(p_box, p_action, p_param)
    local m_box, m_action, m_param = p_box, p_action, p_param

    function self.interact()
        m_action(m_param)
    end

    -- Passtrhrough call to box
    function self.tl()
        return m_box.tl()
    end
    
    -- Passtrhrough call to box
    function self.br()
        return m_box.br()
    end

    -- Passtrhrough call to box
    function self.update(a_x,a_y)
        m_box.update(a_x,a_y)
	end
    
    -- Passtrhrough call to box
    function self.check(a_o,a_xv,a_yv)
        m_box.check(a_o,a_xv,a_yv)
    end

    -- Passtrhrough call to box
    function self.draw(c)
        m_box.draw(c)
    end
end