level=object:extend()

function level:new(na_cx, na_cy, na_sx, na_sy, na_w, na_h)

    local cellx = na_cx
    local celly = na_cy
    local screenx = na_sx
    local screeny = na_sy
    local width = na_w
    local height = na_h

    local m_collision_partition=partition(na_w*8,na_h*8,na_w,na_h)
    local m_trigger_partition=partition(na_w*8,na_h*8,na_w,na_h)

    local player_spawn_position = vector()

    --properties
    function self.player_spawn_position(v)
        if (v) player_spawn_position=v
        return player_spawn_position
    end

    for x=0,na_w-1 do
        for y=0,na_h-1 do
            local sprite = mget(na_cx+x,na_cy+y)
            local collision_flag = fget(sprite, 1)
            local nx=x*8
            local ny=y*8

            if collision_flag then
                m_collision_partition.place(nx, ny, box(nx,ny,8,8))
            end

            if sprite==185 then
                player_spawn_position = vector(nx,ny)
            end
        end
    end

    function self.add_interactable(posx, posy, inter)
        m_trigger_partition.place(posx, posy, inter)
    end

    function self.init()
        collision_partition = m_collision_partition
        trigger_partition = m_trigger_partition
    end

    function self.draw()
        map(cellx, celly, screenx, screeny, width, height, 0x1)
        pal(2,1)
        map(cellx, celly, screenx, screeny, width, height, 0x80)
        pal()
    end

    function self.overdraw()
        map(cellx, celly, screenx, screeny, width, height, 0x4)
    end
end