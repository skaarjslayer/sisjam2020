player_camera=object:extend()

function player_camera:new(pos)

    local position=pos-vector(64, 64)
    local velocity=vector()
    local direction=vector()
    local target_position=vector()

    local move_force = 5
    local mass = 1
    local max_acceleration = 0.2
    local max_speed = 2
    
    function self.update()
        target_position = player_ref.position() - vector(64, 64)

        local diff_vector = target_position - position
        local direction = -diff_vector
        local desired_velocity = direction * vector(move_force, move_force)
        local acceleration = desired_velocity - velocity
        acceleration = acceleration.truncate(max_acceleration)
        acceleration /= vector(mass, mass)

        -- apply acceleration and cap at max speed
        velocity += acceleration
        velocity = velocity.truncate(max_speed)

        if #diff_vector<16 then
            velocity = vector()
        end

        position += velocity
    end

    function self.draw()
        camera(position.x(),position.y())  
    end
end