player=object:extend()

function player:new(spawn_position)

    local position = spawn_position
    local draw_position = spawn_position - vector(4,4)
    local collision_check_position = draw_position - vector(4, 4)
    local direction = vector()
    local velocity = vector()

    local move_force = 5
    local mass = 1
    local max_acceleration = 0.2
    local max_speed = 2

    --local current_cell = nil
    local box_collider = box(0,0,8,8,-4,-4)

    -- interaction
    local interactables = {}

	function self.position(v)
        if (v) position=v
        return position
	end

    function self.update()

        -- convert player input into direction vector
        local dx = btn(0) and -1 or btn(1) and 1 or 0
        local dy = btn(2) and -1 or btn(3) and 1 or 0
        direction = vector(dx, dy)
        
        if #interactables > 0 and btnp(4) then
            interactables[1].interact()
			return
        end
        -- calculate desired velocity and acceleration
        local desired_velocity = direction * vector(move_force, move_force)
        local acceleration = desired_velocity - velocity
        acceleration = acceleration.truncate(max_acceleration)
        acceleration /= vector(mass, mass)

        -- apply acceleration and cap at max speed
        velocity += acceleration
        velocity = velocity.truncate(max_speed)

        -- check for collisions/triggers
        collision_partition.act(collision_check_position.x(),collision_check_position.y(),
            function(obj)
                if obj~=box_collider then
                    local hit_x = box_collider.check(obj, velocity.x(), 0)
                    local hit_y = box_collider.check(obj, 0, velocity.y())

                    if (hit_x) velocity.x(0)
                    if (hit_y) velocity.y(0)
                end
            end)
        
        interactables = {}
		trigger_partition.act(collision_check_position.x(),collision_check_position.y(),
            function(obj)
				local hit = box_collider.check(obj, velocity.x(), velocity.y())
				if hit then add(interactables, obj) end
			end)

        -- apply velocity to position and update collider
        position += velocity
        draw_position = position - vector(4,4)
        collision_check_position = draw_position - vector(4,4)
        box_collider.update(position.x(), position.y())
        --current_cell = collision_partition.place(position.x(), position.y(), box_collider, current_cell)
    end

    function self.draw()
        spr(167, draw_position.x(), draw_position.y())
    end
end