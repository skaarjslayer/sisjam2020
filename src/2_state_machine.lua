state_machine=object:extend()

function state_machine:new()

	-- vars
	local m_sts, m_cst = {}, nil

	-- properties
	function self.current_state()
		return m_cst
	end

	-- functions
	function self.update()
		if m_cst then
			if m_cst.update then
				m_cst.update()
			end
		end
	end
	
	function self.draw()
		if m_cst then
			if m_cst.draw then
				m_cst.draw()
			end
		end
	end
	
	function self.push(a_nst)
		if m_cst then
			if m_cst.disable then
				m_cst.disable()
			end
		end
	
		m_sts[#m_sts + 1]=a_nst
		m_cst=a_nst
	
		if m_cst.enter then
			m_cst.enter()
		end

		if m_cst.enable then
			m_cst.enable()
		end
	end
	
	function self.advance(a_nst)
		if m_cst then
			if m_cst.disable then
				m_cst.disable()
			end

			if m_cst.exit then
				m_cst.exit()
			end

			del(m_sts, m_cst)
		end
	
		m_sts[#m_sts + 1]=a_nst
		m_cst=a_nst
	
		if m_cst.enter then
			m_cst.enter()
		end

		if m_cst.enable then
			m_cst.enable()
		end
	end
end