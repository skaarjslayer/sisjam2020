local object={}
object.__index=object

function object:extend()
	local new={}
	for k, v in pairs(self) do
		if sub(k,1,2)=="__" then
			new[k] = v
		end
	end
	new.__index = new
	setmetatable(new, self)
	return new
end

function object:__call(...)
	local obj = setmetatable({}, self)
	obj:new(...)
	return obj
end

function object:merge(table)
	for k,v in pairs(table) do
		self[k] = v
	end
end