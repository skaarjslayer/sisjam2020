import os

def append_cart(source_folder, destination_filepath):
	for dirpath, dirs, files in os.walk(source_folder):
		for filename in files:
			fname = os.path.join(dirpath,filename)
			copy_all_lines(fname,destination_filepath)
		
def copy_all_lines(source_filepath, destination_filepath):
	source=open(source_filepath)
	destination=open(destination_filepath, "a")

	for line in source:
		line = line.replace('\t','')
		line = line.replace('\r','')
		if line.count(line[0]) == len(line):
			if line[0] == '\n':
				line = line.replace('\n','')
		destination.write(line)
	destination.write('\n')
	source.close()
	destination.close()
	
append_cart("..\\src\\","..\\bin\\sisjam2020_build.lua")